//jsPlumb.ready(function() {
var instance = null;
var jsonStates = null;
var ddData = [
    {
        text: "normal",
        value: 1,
        selected: true,
        imageSrc: "../images/check.png"
    },
    {
        text: "error",
        value: 2,
        selected: false,
        imageSrc: "../images/error.png"
    }];

var draw = function(xmlData){
    var x2js = new X2JS();
    jsonStates = x2js.xml_str2json(xmlData);
    var statesXML = new XElement.parse(xmlData);
    //    statesXML.descendantNodes()
    var statesAndAlternateStates = {}; //probably this will aid in validation - report duplicate states
    /*
      {
      "SEARCH_SUBSCRIBER" : ["ENDPROCESS_TRANSFER", "VALIDATE_FCC"]
      }

      {
      "SEARCH_SUBSCRIBER" : {
      error: {
      "ENDPROCESS_TRANSFER" : ["true", "true || false"],
      "VALIDATE_FCC" :        [true]
      },
      normal : {
      "ENDPROCESS_TRANSFER" : ["true", "true || false"],
      "VALIDATE_FCC" :        [true]			        
      }
      },
      }
      _.chain(j.s.endaction)
      .groupBy(function(i){if(i._type == undefined){return "normal"} return i._type;})
      .map(function(v, k){return {type : k, conds : _.groupBy(v[0].al, function(o){return o._g})};})
      .value()

      [{"type":"error","conds":{"s1":[{"_g":"s1","_c":"c1"},{"_g":"s1","_c":"c2"}],"s2":[{"_g":"s2","_c":"c1"},{"_g":"s2","_c":"c2"}],"s3":[{"_g":"s3","_c":"c1"}]}},{"type":"normal","conds":{"s1":[{"_g":"s1","_c":"c1"},{"_g":"s1","_c":"c2"}],"s2":[{"_g":"s2","_c":"c1"},{"_g":"s2","_c":"c2"}],"s3":[{"_g":"s3","_c":"c1"}]}}]
      
      [{"type":"error","conds":{}},{"type":"normal","conds":{"s1":[{"_g":"s1","_c":"c1"},{"_g":"s1","_c":"c2"}],"s2":[{"_g":"s2","_c":"c1"},{"_g":"s2","_c":"c2"}],"s3":[{"_g":"s3","_c":"c1"}]}}]       
    */

    var mainDiv = document.getElementById("statemachine-demo");
    //statesXML.elements("state").forEach(function(state){
    //for laying out the states on the canvas
    var nextOffset = 0;
    var topOffset = 0;
    jsonStates.States.State.forEach(function(state){
	var stateName = state._Name;//cast(state.attribute('name'));
	//TODO: replace this xml version with JSON version 
	/*statesAndAlternateStates[stateName] = state.element("endactions").elements("endaction")
          .where(function(action){return !action.hasAttributes;}) //non-error endaction section
          .first()
	  .nodesArray
          .filter(function(e){return e.nodeType == "Element"})
          .map(function(als){return cast(als.attribute("goto"))});
	*/
	statesAndAlternateStates[stateName] = _.chain(state.EndActions.EndAction).groupBy(function(i) {
	    if (i._Type == undefined) {
		return "normal"
	    }
	    return i._Type;
	}).map(function(v, k) {
	    if (v[0].AlternateState != null) {
		if (_.isArray(v[0].AlternateState)) {
		    return {
			type: k,
			conds: _.groupBy(v[0].AlternateState, function(o) {
			    return o._GotoState;
			})
		    };
		} else {
		    var s = v[0].AlternateState._GotoState;
		    var o = {};
		    o[s] = [{
			_Condition: v[0].AlternateState._Condition,
			_GotoState: v[0].AlternateState._GotoState
		    }];
		    return {
			type: k,
			conds: o
		    };
		}
	    } else {
		return {}
	    }
	}).value();

	var div = document.createElement('div');
	div.className = 'w';
	div.id = stateName; 

	div.innerHTML = '<div id="state_name">' + div.id + '</div><div class="ep"></div>';
	mainDiv.appendChild(div);

	//if(mainDiv.width - nextOffset < somevalue) set nextOffset = 0 and add {top : stateHeight + someoffset, left: nextOffset} to css 
	//and recalculate nextOffset
	
	if(nextOffset > mainDiv.clientWidth || mainDiv.clientWidth - nextOffset < 50){
	    nextOffset = 0;
	    topOffset += 100;
	}
	$(div).css({left : nextOffset, top : topOffset});
	nextOffset += div.offsetWidth + 50;
    });//foreach ends

    $('div[id="state_name"]').editable(function(value, settings) { 
	return(value);
    });

    //<div class="w" id="SEARCH_MDN">SEARCH_MDN<div class="ep"></div></div>

    // setup some defaults for jsPlumb.
    instance = jsPlumb.getInstance({
	Endpoint : ["Dot", {radius:2}],
	HoverPaintStyle : {strokeStyle:"#1e8151", lineWidth:2 },
	ConnectionOverlays : [
	    [ "Arrow", {
		location:1,
		id:"arrow",
                length:14,
                foldback:0.8
	    } ],
	    ["Custom", {
		create:function(component) {
		    return $("<select id='myDropDown'><option value='normal'>n</option><option value='error'>e</option></select>");  
		    //return $("<div id='demoDefaultSelection'/>"); 
		},
		location:0.7,
		id:"customOverlay"
	    }]
            //,[ "Label", { label:"FOO", id:"label", cssClass:"aLabel" }]
	    ,["Custom",{
		create:function(component){
		    return $("<div id='trip'>true</div>");
		},
		location:0.5,
		id:"conditionOverlay"
	    }]
	]
	,Container:"statemachine-demo"
    });

    var windows = jsPlumb.getSelector(".statemachine-demo .w");

    // initialise draggable elements.
    instance.draggable(windows);

    // bind a click listener to each connection; the connection is deleted. you could of course
    // just do this: jsPlumb.bind("click", jsPlumb.detach), but I wanted to make it clear what was
    // happening.
    instance.bind("dblclick", function(c) {
	//confirm first
	confirm('Sure?', function(yes) {
            if (yes) {
		instance.detach(c);
            }
        });
    });

    // bind a connection listener. note that the parameter passed to this function contains more than
    // just the new connection - see the documentation for a full list of what is included in 'info'.
    instance.bind("connection", function(info, e) {
	//info.connection.getOverlay("label").setLabel('true'/*info.connection.id*/);
	if(e){ //mouse
	    var c = info.connection;
	    var newId = "co_" + c.sourceId + "_to_" + c.targetId +  uuid.v4(); //we must generate unique ids for editable to work; array will not do because overlays containing ... will also become editable
	    c.getOverlay('conditionOverlay').canvas.id = newId;
	    $('#' + newId).editable(function(value, settings) { 
		//console.log(this); console.log(value); console.log(settings);
		return value;
	    });
    	}else{ //programatic
    	}
    });


    // suspend drawing and initialise.
    instance.doWhileSuspended(function() {
	var isFilterSupported = instance.isDragFilterSupported();
	// make each ".ep" div a source and give it some parameters to work with.  here we tell it
	// to use a Continuous anchor and the StateMachine connectors, and also we give it the
	// connector's paint style.  note that in this demo the strokeStyle is dynamically generated,
	// which prevents us from just setting a jsPlumb.Defaults.PaintStyle.  but that is what i
	// would recommend you do. Note also here that we use the 'filter' option to tell jsPlumb
	// which parts of the element should actually respond to a drag start.
	// here we test the capabilities of the library, to see if we
	// can provide a `filter` (our preference, support by vanilla
	// jsPlumb and the jQuery version), or if that is not supported,
	// a `parent` (YUI and MooTools). I want to make it perfectly
	// clear that `filter` is better. Use filter when you can.
	if (isFilterSupported) {
	    instance.makeSource(windows, {
		filter:".ep",
		anchor:"Continuous",
		connector:[ "StateMachine", { curviness:20 } ],
		connectorStyle:{ strokeStyle:"#5c96bc", lineWidth:2, outlineColor:"transparent", outlineWidth:4 },
		maxConnections:5,
		onMaxConnections:function(info, e) {
		    alert("Maximum connections (" + info.maxConnections + ") reached");
		}
	    });
	}
	else {
	    var eps = jsPlumb.getSelector(".ep");
	    for (var i = 0; i < eps.length; i++) {
		var e = eps[i], p = e.parentNode;
		instance.makeSource(e, {
		    parent:p,
		    anchor:"Continuous",
		    connector:[ "StateMachine", { curviness:20 } ],
		    connectorStyle:{ strokeStyle:"#5c96bc",lineWidth:2, outlineColor:"transparent", outlineWidth:4 },
		    maxConnections:5,
		    onMaxConnections:function(info, e) {
			alert("Maximum connections (" + info.maxConnections + ") reached");
		    }
		});
	    }
	}
    });

    // initialise all '.w' elements as connection targets.
    instance.makeTarget(windows, {
	dropOptions:{ hoverClass:"dragHover" },
	anchor:"Continuous",
	allowLoopback:true,
	anchor:"Continuous"
    });

    // and finally, make a couple of connections

    for(var key in statesAndAlternateStates){
	statesAndAlternateStates[key].forEach(function(endaction){
	    if(!_.isEmpty(endaction) && endaction.type != "ERROR"){
		for(var _key in endaction.conds){ //also check the type of endaction and then set the connector properties accordingly - red arrow for error?
		    var c = instance.connect({source:key, target:_key}); 
		    
		    //modify the div id here so that evey condition gets its own tooltip content
		    var newId = "co_" + c.sourceId + "_to_" + c.targetId;
		    c.getOverlay('conditionOverlay').canvas.id = newId;
		    
		    //get all the conditions of the goto state and create a <ul> list to show it on the tooltip
		    var textContent = '<ul id="conditionsList">';
		    endaction.conds[_key].forEach(function(o){
			textContent += '<div><li id="tt_'+newId+'">' + o._Condition + '</li><img id="remove" src="../images/minus.png"/></div>';
		    }); 
		    textContent += '</ul>';

		    
		    //set the overlay text
		    var condition = $(textContent).children()[0].childNodes[0].innerText;
		    if(condition.length > 4){
			c.getOverlay('conditionOverlay').canvas.textContent = '...';
		    }else{
			c.getOverlay('conditionOverlay').canvas.textContent = condition;
		    	$('#' + newId).editable(function(value, settings) { 
		    	    //console.log(this); console.log(value); console.log(settings);
		    	    return value;
		    	});
		    }

		    //TODO : temporary commented
		    // if(textContent.length > 4){ //if the condition is long, then let the user see it on the tooltip
		    // 	c.getOverlay('conditionOverlay').canvas.textContent = '...';//endaction.conds[_key][0]._condition; 
		    // }else{
		    // 	c.getOverlay('conditionOverlay').canvas.textContent = textContent;
		    // 	$('#' + newId).editable(function(value, settings) { 
		    // 	    //console.log(this); console.log(value); console.log(settings);
		    // 	    return value;
		    // 	});
		    // }
		    
		    $('#' + newId).qtip({ //show an editable tooltip over a condition
			//content: '<div id="tt_'+newId+'">' + textContent+ '</div>',
			content: '<img src="../images/add.png"><br/>' + textContent,
			hide: {
			    fixed: true,
			    delay: 300
			},
			events: {
			    render: function() {
				// var content = api.elements.content;
				//the following editable is going to be conditional - dont set editable if its a <ul>
				$('[id="tt_' + newId + '"]').editable(function(value, settings) { 
				    //console.log(this); console.log(value); 
				    
				    console.log(settings);
				    // if(value.length > 4)
				    // 	c.getOverlay('conditionOverlay').canvas.textContent = '...';
				    // else{
				    // 	c.getOverlay('conditionOverlay').canvas.textContent = value; //set the new text as the underlying overlay's value
				    // 	$('#' + newId).editable(function(value, settings) { //set the overlay as editable now that the size is within the range
				    // 	    return value;
				    // 	});
				    // }
				    return value;
				});

				//Add click events for the remove buttons
				$("[id='conditionsList']").on("click", "#remove", function () {
				    $(this).parent('div').remove();
				});
			    }
			}
		    }); //qtip ends
		}//for ends
	    }//if ends
	    //instance.getConnections({source:'SEARCH_MDN'})[0].getOverlay('conditionOverlay').canvas !!!!!access inner div of the overlay
	    var a = 3;
	});//forEach ends
    }

    jsPlumb.fire("jsPlumbDemoLoaded", instance);

}

$("#generate").on("click", function(e) {
    
    if(instance != null){
	var xmlData = "";
	//get all the changes made to the state chart
        jsonStates["states"]["state"].forEach(function(s){
	    instance.getConnections({source:s._name}).forEach(function(c){
		if($.grep(s["EndActions"]["EndAction"], function(i){return i._type == null && i.AlternateState != null && i.AlternateState._goto == c.targetId}).length == 0){
		    //we dont want to add another alternatestate object; we want to turn alternatestate into an array and then add all the
		    //alternatestates' attributes in the same array:
		    /*
		      {alternatestate : [{goto:'...', condition:'...'}, {goto:'...', condition:'...'}]}
		    */
		    //This is for generating a common parent tag <endaction> for the non-error endactions
		    if(!$.isArray(s["EndActions"]["EndAction"][s["EndActions"]["EndAction"].length-1].AlternateState)){
			var a = $.makeArray(s["EndActions"]["EndAction"][s["EndActions"]["EndAction"].length-1].AlternateState);
			s["EndActions"]["EndAction"][s["EndActions"]["EndAction"].length-1]['AlternateState'] = a;
		    }
		    s["EndActions"]["EndAction"][s["EndActions"]["EndAction"].length-1]['AlternateState'].push({ "_GotoState" : c.targetId, "_Condition" : "true"});
		}
		
	    });//forEach ends
            
	});
	xmlData = (new X2JS).json2xml_str(jsonStates);
	console.log(xmlData);
	
	$("#dataLink").attr("href", 'data:Application/octet-stream,' + encodeURIComponent(xmlData))[0].click();
	
        alert('done');
    }
    
});

// $(document).ready(function () {

// });
$('#add').on('click', function(e){
    var mainDiv = document.getElementById("statemachine-demo");
    var div = document.createElement('div');
    div.className = 'w';
    div.id = 'S10'; 

    div.innerHTML = '<div id="state_name">' + div.id + '</div><div class="ep"></div>';
    mainDiv.appendChild(div);
    $('div[id="state_name"]').editable(function(value, settings) { 
	return(value);
    });

    // make this new div draggable
    var windows = jsPlumb.getSelector(".statemachine-demo .w");
    // initialise draggable elements.
    instance.draggable(windows);

    var eps = jsPlumb.getSelector(".ep");
    for (var i = 0; i < eps.length; i++) {
	var e = eps[i], p = e.parentNode;
	instance.makeSource(e, {
	    parent:p,
	    anchor:"Continuous",
	    connector:[ "StateMachine", { curviness:20 } ],
	    connectorStyle:{ strokeStyle:"#5c96bc",lineWidth:2, outlineColor:"transparent", outlineWidth:4 },
	    maxConnections:5,
	    onMaxConnections:function(info, e) {
		alert("Maximum connections (" + info.maxConnections + ") reached");
	    }
	});
    }


    // initialise all '.w' elements as connection targets.
    instance.makeTarget(windows, {
	dropOptions:{ hoverClass:"dragHover" },
	anchor:"Continuous",
	allowLoopback:true,
	anchor:"Continuous"
    });

   


});


function confirm(question, callback) {
    // Content will consist of the question and ok/cancel buttons
    var message = $('<p />', {
        text: question
    }),
    ok = $('<button />', {
        text: 'Ok',
        click: function() {
            callback(true);
        }
    }),
    cancel = $('<button />', {
        text: 'Cancel',
        click: function() {
            callback(false);
        }
    });

    dialogue(message.add(ok).add(cancel), 'Delete connection');
}

function dialogue(content, title) {
    $('<div />').qtip({
        content: {
            text: content,
            title: title
        },
        position: {
            my: 'center', at: 'center',
            target: $(window)
        },
        show: {
            ready: true,
            modal: {
                on: true,
                blur: false
            }
        },
        hide: false,
        style: 'dialogue',
        events: {
            render: function(event, api) {
                $('button', api.elements.content).click(function(e) {
                    api.hide(e);
                });
            },
            hide: function(event, api) { api.destroy(); }
        }
    });
}
