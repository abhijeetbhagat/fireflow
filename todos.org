** TODO handle error states
** TODO validate duplicate states
** IN-PROGRESS show multiple conditions for an alternate state in an editable tooltip
** TODO adding, removing states should modify the underlying jsonStates object
** TODO positioning of the boxes on the canvas
** TODO pop-ups for modifying the state info - captured-data, behaviors, etc
** TODO possibly edit the state name on a double click
** DONE generate different ids for the condition overlays so that tool-tips data will be different. This might mean changing the div's id: http://stackoverflow.com/questions/347798/changing-an-elements-id-with-jquery
** DONE confirm connection deletion through a dialog
** TODO delete the connection from jsonStates
** TODO generate(): change the string properties

*** If a state has multiple transitions to the same state, then show ... on the overlay and show a list of editable conditions on the tooltip but dont make the overlay itself editable
    If a state has single transition to a state, then check the length of the condition:
        if the length is more than 4 characters, then show ... on the overlay and show the condition on the tooltip but dont make the overlay itself editable
	else, show the condition on the overlay as well as the tooltip and make the overlay itself editable

    If after editing the condition(s) on the tip, if there remains only a single condition with length 4 characters, then change the overlay content to the new condition
